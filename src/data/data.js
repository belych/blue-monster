
const DATA = {
    bodies: ['./images/body/blue.svg', './images/body/pink.svg', './images/body/green.svg'],
    faces: [
        [
            './images/face/blue_face/Group 83.svg',
            './images/face/blue_face/Group 86.svg',
            './images/face/blue_face/Group 87.svg',
        ],
        [
            './images/face/green_face/Group 88.svg',
            './images/face/green_face/Group 89.svg',
            './images/face/green_face/Group 90.svg',
        ],
        [
            './images/face/pink_face/Group 91.svg',
            './images/face/pink_face/Group 94.svg',
            './images/face/pink_face/Group 95.svg',
        ]
    ]
}

export default DATA;



