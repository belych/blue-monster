const GRADES = [
    ['HTML',
        'CSS',
        'Adaptive',
        'Base JS',
        'SQL',
        'Git',
        'ESLint',
        'SASS',
        'Pug',
        'Gulp',
        'Webpack'
    ],
    ['TDD',
        'JS',
        'Canvas',
        'SVG',
        'Bash',
        'CI/CD',
        'React',
        'D3.js',
        'Vue',
        'SPA',
        'ES6'
    ],
    ['ThreeJs',
        'WebGL',
        'Chrome Extensions',
        'Node',
        'Express',
        'Architecture',
    ]
]
export default GRADES;