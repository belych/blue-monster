import React, { Component } from 'react';
import './Monster.scss';
import GRADES from '../../data/grades';
import Grade from '../Grade/Grade';

class Monster extends Component {
    render() {
        return (
            <div className='drades-monster'>
            <Grade grades={GRADES} grade_active={this.props.grade_active}/>
            <div className='monster'>
                <div className='moster__container-body-img'>
                    <img className='monster__body-img' src={this.props.body_active} alt='monster' />
                </div>
                <div className='moster__container-face-img'>
                    <img className='monster__face-img' src={this.props.face_active} alt='monster' />
                </div>
                <div className='moster__container-fable-img'>
                    <img className='monster__table-img' src='./images/table.svg' alt='monster' />
                </div>
            </div>
            </div>
        )
    }
}

export default Monster;