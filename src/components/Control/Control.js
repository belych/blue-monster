import React, { Component } from 'react';
import './Control.scss';

class Control extends Component{
    state ={
        grade_active: ''
    }
    handleClickInput = (e)=>{
        this.setState({ grade_active: e.currentTarget.value});
        this.props.updateGrade(e.currentTarget.value);
    }
    render(){
        return(
            <div className='control'>
                <div className='control__header'>Грейды для frontend</div>
                <div className='control__form'>
                    <input className='input' onChange={this.handleClickInput} value={this.state.grade} type="range" min="0" max="2"/> 
                    <div className='control__value'>
                        <div>Junior</div>
                        <div>Middle</div>
                        <div>Senior</div>
                    </div>
                
                <div className='control__text'>
                    <img className='control__img' src='images/quotes.png' alt='quotes'/>
                    <div>Переход на следующий грейд - улучшение качества кода и увеличение ответственности.</div>
                </div>
                </div>
            </div>
        )
    }
}

export default Control;