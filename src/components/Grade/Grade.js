import React, { Component } from 'react';
import './Grade.scss'

class Grade extends Component {

    handelClickGrade = () => {
        const { grade_active } = this.props;
        const { grades } = this.props;
        let grade = [];
        for (let i = 0; i <= grade_active; i++) {
            for (let j = 0; j <= grades[i].length - 1; j++) {
                let size = 25*Math.random();
                size=size <13 ? 13: size ;
                const padding = `${size/10}px ${size/3}px`;
                grade.push(<div style={{fontSize: (size),
                     padding: padding }} key={grades[i][j]}
                    className='grades__grade'>{grades[i][j]}</div>);
            }
        }
        return grade;
    }

    render() {
        return (
            <div className='grades'>{this.handelClickGrade()}</div>
        )
    }
}

export default Grade;