import React, { Component } from 'react';
import Control from '../Control/Control';
import Constructor from '../Constructor/Constructor';
import './Main.scss';


class Main extends Component {
    state = {
        bodyIndex: 0,
        face_active: '',
        grade: 0,
    }
    handleFace = (e) => {
        this.props.updateDataFace(e.currentTarget.alt);
    }
    handleBody = (e) => {
        const idx = e.target.dataset.index
        this.setState({ bodyIndex: idx });
        this.props.updateDataBody(e.currentTarget.alt);
        this.props.updateDataFace(this.props.data.faces[idx][0]);
    }
    updateGrade = (value) => {
        this.setState({ grade_active: value })
        this.props.updateGrade(value);
    }
    render() {
        return (
            <div className='main'>
                <Control updateGrade={this.updateGrade} />
                <div className='constructors'>
                    <Constructor data={this.props.data.bodies}
                        onClick={this.handleBody}
                        text='Тело'
                    />
                    <Constructor data={this.props.data.faces[this.state.bodyIndex]}
                        onClick={this.handleFace}
                        text='Лицо' />
                </div>
            </div>
        )
    }
}

export default Main;