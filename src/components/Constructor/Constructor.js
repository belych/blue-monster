import React, { Component } from 'react';
import './Constructor.scss';

class Constructor extends Component {
    render() {
        return (
            <div className='constructor'>
                <div className='constructor__text'>{this.props.text}</div>
                <div className='constructor__container'>
                    {this.props.data.map((item, idx) => {
                        return <div key={item} className='constructor__container-img'>
                            <img onClick={this.props.onClick}
                                className='constructor__img'
                                src={item}
                                alt={item}
                                data-index={idx} />
                        </div>
                    })}
                </div>
            </div>
        )
    }
}

export default Constructor;