import React, { Component } from 'react';
import './index.scss';
import Monster from './components/Monster/Monster';
import Main from './components/Main/Main';
import DATA from '../src/data/data';
import GRADES from './data/grades';

class App extends Component {
  state = {
    body_active: './images/body/blue.svg',
    face_active: './images/face/blue_face/Group 83.svg',
    grade_active: 1
  }

  updateDataBody = (value) => {
    this.setState({ body_active: value })
  }
  updateDataFace = (value) => {
    this.setState({ face_active: value })
  }
  updateGrade = (value) => {
    this.setState({grade_active: value})
  }
  render() {
    return (
      <div className="App">
        <Main data={DATA} updateDataBody={this.updateDataBody} updateDataFace={this.updateDataFace} updateGrade={this.updateGrade}/>
        <Monster body_active={this.state.body_active} face_active={this.state.face_active} grades={GRADES} grade_active={this.state.grade_active} />
      </div>
    );
  }
}

export default App;
